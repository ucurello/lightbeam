using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Common.Contracts
{
    public interface IBusinessManagerFactory
    {
        T GetBusinessManager<T>() where T : IBusinessManager;
    }
}
