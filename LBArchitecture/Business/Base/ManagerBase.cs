﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using System.Diagnostics;

using Core.Common.Core;
using System.Runtime.CompilerServices;
using Core.Common.Contracts;

namespace LightBeam.Business
{
    public class ManagerBase : IBusinessManager
    {
        readonly log4net.ILog _log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ManagerBase()
        {
            if (ObjectBase.Container != null)
                ObjectBase.Container.SatisfyImportsOnce(this);
        }

        protected T ExecuteErrorHandledOperation<T>(Func<T> codeToExecute, [CallerMemberName] string memberName = "")
        {
            var stopWatch = new Stopwatch();
            try
            {
                stopWatch.Start();
                return codeToExecute.Invoke();
            }
            catch (Exception ex)
            {
                _log.Error(string.Format("[{0}]", memberName), ex);
                throw ex;
            }
            finally
            {
                _log.Debug(string.Format("[{0}]-Elapsed time {1} ms", memberName, stopWatch.ElapsedMilliseconds));
            }
        }

        protected void ExecuteErrorHandledOperation(Action codeToExecute, [CallerMemberName] string memberName = "")
        {
            var stopWatch = new Stopwatch();
            try
            {
                codeToExecute.Invoke();
            }
            catch (Exception ex)
            {
                _log.Error(string.Format("[{0}]", memberName), ex);
                throw ex;
            }
            finally
            {
                _log.Debug(string.Format("[{0}]-Elapsed time {1} ms", memberName, stopWatch.ElapsedMilliseconds));
            }
        }
    }
}
