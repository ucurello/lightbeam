﻿using Core.Common.Contracts;
using Core.Common.Core;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightBeam.Business
{
    [Export(typeof(IBusinessManagerFactory))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class ManagerFactory : IBusinessManagerFactory
    {
        T IBusinessManagerFactory.GetBusinessManager<T>()
        {
            return ObjectBase.Container.GetExportedValue<T>();
        }
    }
}
