﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;

using LightBeam.Data.Repositories;
using LightBeam.Business.Managers;

namespace LightBeam.Business.Bootstrapper
{
    public static class MEFLoader
    {

        public static CompositionContainer Init()
        {
            var catalog = new AggregateCatalog();
            // Add items to catalog here
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(UserMasterRepository).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(SecurityService).Assembly));
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(ManagerFactory).Assembly));

            var container = new CompositionContainer(catalog);
            return container;
        }
    }
}
