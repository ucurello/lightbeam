﻿using Core.Common.Contracts;
using System;
using System.Collections.Generic;


namespace LightBeam.Business.Contracts.Security
{
    public interface ISecurityService : IBusinessManager
    {
        LightBeam.Business.Entities.Security.UserMaster CreateUser(string createdBy, LightBeam.Business.Entities.Security.UserMaster user);
        LightBeam.Business.Entities.Security.RoleMaster GetRole(int id);
        IEnumerable<LightBeam.Business.Entities.Security.RoleMaster> GetRoles();
        LightBeam.Business.Entities.Security.UserMaster GetUser(int userId);
        LightBeam.Business.Entities.Security.UserMaster GetUserByLogin(string username, string password);
        IEnumerable<LightBeam.Business.Entities.Security.RoleMaster> GetUserRoles(int userId);
        IEnumerable<LightBeam.Business.Entities.Security.RoleMaster> GetUserRoles(string username);
        IEnumerable<LightBeam.Business.Entities.Security.UserMaster> GetUsers();
        LightBeam.Business.Entities.Security.UserMaster UpdateUser(string modifiedBy, LightBeam.Business.Entities.Security.UserMaster user);
    }
}
