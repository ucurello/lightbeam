﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

using AutoMapper;
using Core.Common.Contracts;
using LightBeam.Business.Contracts.Security;
using LightBeam.Data.Contracts.Repository_Interfaces;

using RoleMaster = LightBeam.Business.Entities.Security.RoleMaster;
using UserMaster = LightBeam.Business.Entities.Security.UserMaster;

namespace LightBeam.Business.Managers
{

    public class TestService
    {
        IDataRepositoryFactory _dataRepositoryFactory;

        public TestService(IDataRepositoryFactory dataRepositoryFactory)
        {
            _dataRepositoryFactory = dataRepositoryFactory;
        }

        public void TestCreate(LightBeam.Data.Entities.UserMaster user)
        {
            // Instantiate the security service and call the GetUsers method
            var myUserRepo = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            user = myUserRepo.Add(user);
        }

        public UserMaster CreateUser(UserMaster user, LightBeam.Data.Entities.UserMaster user2)
        {
            Mapper.CreateMap<Data.Entities.UserMaster, UserMaster>();
            Mapper.CreateMap<UserMaster, Data.Entities.UserMaster>();

            var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();

            var newUser = userMasterRepository.Add(Mapper.Map<UserMaster, Data.Entities.UserMaster>(user));
            var newUser2 = userMasterRepository.Add(user2);

            var userDTO = Mapper.Map<Data.Entities.UserMaster, UserMaster>(newUser);
            return userDTO;
        }

    }

    [Export(typeof(ISecurityService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SecurityService : ManagerBase, ISecurityService
    {
        private static bool _mapperInitialized = false;
        private static readonly object _lockObject = new object();

        [Import]
        IDataRepositoryFactory _dataRepositoryFactory;

        public SecurityService() : base()
        {
            lock (_lockObject)
            {
                if (!_mapperInitialized)
                {
                    Mapper.CreateMap<Data.Entities.UserMaster, UserMaster>();
                    Mapper.CreateMap<UserMaster, Data.Entities.UserMaster>();
                    Mapper.CreateMap<Data.Entities.RoleMaster, RoleMaster>();
                    Mapper.CreateMap<RoleMaster, Data.Entities.RoleMaster>();
                    _mapperInitialized = true;
                }
            }
        }

        public SecurityService(IDataRepositoryFactory dataRepositoryFactory)
        {
            lock (_lockObject)
            {
                if (!_mapperInitialized)
                {
                    Mapper.CreateMap<Data.Entities.UserMaster, UserMaster>();
                    Mapper.CreateMap<UserMaster, Data.Entities.UserMaster>();
                    Mapper.CreateMap<Data.Entities.RoleMaster, RoleMaster>();
                    Mapper.CreateMap<RoleMaster, Data.Entities.RoleMaster>();
                    _mapperInitialized = true;
                }
            }
            _dataRepositoryFactory = dataRepositoryFactory;
        }

        public IEnumerable<UserMaster> GetUsers()
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var users = userMasterRepository.Get();
                var usersDTO = Mapper.Map<IEnumerable<Data.Entities.UserMaster>, IEnumerable<UserMaster>>(users);
                return usersDTO;
            });         
        }

        public UserMaster GetUser(int userId)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var user = userMasterRepository.Get(userId);
                var userDTO = Mapper.Map<Data.Entities.UserMaster, UserMaster>(user);
                return userDTO;
            });
        }

        public UserMaster GetUserByLogin(string username, string password)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var user = userMasterRepository.Get(username, password);
                var userDTO = Mapper.Map<Data.Entities.UserMaster, UserMaster>(user);
                return userDTO;
            });
        }

        public UserMaster CreateUser(string createdBy, UserMaster user)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                user.CreatedBy = createdBy;
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var newUser = userMasterRepository.Add(Mapper.Map<UserMaster, Data.Entities.UserMaster>(user));
                var userDTO = Mapper.Map<Data.Entities.UserMaster, UserMaster>(newUser);
                return userDTO;
            });
        }

        public UserMaster UpdateUser(string modifiedBy, UserMaster user)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var updatedUser = Mapper.Map<UserMaster, Data.Entities.UserMaster>(user);
                user.ModifiedBy = modifiedBy;
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var userDTO = Mapper.Map<Data.Entities.UserMaster, UserMaster>(userMasterRepository.Update(updatedUser));
                return userDTO;
            });
        }

        public IEnumerable<RoleMaster> GetRoles()
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var roleMasterRepository = _dataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();
                var roles = roleMasterRepository.Get();
                return Mapper.Map<IEnumerable<Data.Entities.RoleMaster>, IEnumerable<RoleMaster>>(roles);
            });
        }

        public RoleMaster GetRole(int id)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var roleMasterRepository = _dataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();
                var role = roleMasterRepository.Get(id);
                return Mapper.Map<Data.Entities.RoleMaster, RoleMaster>(role);
            });
        }

        public IEnumerable<RoleMaster> GetUserRoles(int userId)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var roles = userMasterRepository.GetRoles(userId);
                var rolesDTO = Mapper.Map<List<Data.Entities.RoleMaster>, List<RoleMaster>>(roles.ToList());
                return rolesDTO;
            });
        }

        public IEnumerable<RoleMaster> GetUserRoles(string username)
        {
            return ExecuteErrorHandledOperation(() =>
            {
                var userMasterRepository = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
                var roles = userMasterRepository.GetRoles(username);
                var rolesDTO = Mapper.Map<List<Data.Entities.RoleMaster>, List<RoleMaster>>(roles.ToList());
                return rolesDTO;
            });
        }
    }
}
