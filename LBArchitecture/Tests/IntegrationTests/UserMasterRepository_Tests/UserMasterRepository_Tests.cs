﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Data.Entity;
using NUnit.Framework;
using Core.Common.Contracts;
using Core.Common.Core;
using LightBeam.Business.Bootstrapper;
using LightBeam.Data.Contracts.Repository_Interfaces;
using LightBeam.Data.Entities;

namespace LightBeam.Data.Tests.UserMasterRepositoryTest
{
    [TestFixture]
    public class UserMasterRepository_Tests 
    {
        RepositoryFactoryTestClass repositoryTest;
        UserMaster user;
        UserMaster user2;
        [TestFixtureSetUp]
        public void Initialize()
        {
            ObjectBase.Container = MEFLoader.Init();

            repositoryTest = new RepositoryFactoryTestClass();
            user = new UserMaster()
            {
                FirstName = "Giuseppe",
                LastName = "Puglisi",
                Username = "gpuglisi",
                EmailAddress = "gpuglisi@yahoo.com",
                Password = "gppassword",
                CreatedBy = "ucurello"
            };
            user2 = new UserMaster()
            {
                FirstName = "Claudio",
                LastName = "Moretti",
                Username = "cmoretti",
                EmailAddress = "cmoretti@yahoo.com",
                Password = "cmpassword",
                CreatedBy = "ucurello"
            };
        }

        [TestFixtureTearDown]
        public void CleanUp()
        {
 
        }

        [Test]
        public void test_add_users()
        {
            user = repositoryTest.AddUser(user);
            user2 = repositoryTest.AddUser(user2);
            var users = repositoryTest.GetUsers();
            Assert.IsTrue(users.Count<UserMaster>() >= 2);
            repositoryTest.RemoveUser(user.UserMasterId);
            repositoryTest.RemoveUser(user2.UserMasterId);
        }

        [Test]
        public void test_add_user_with_duplicate_username()
        {
            repositoryTest.AddUser(user);
            //var newUser = repositoryTest.AddUser(user);
            Exception ex = Assert.Throws(typeof(System.Data.Entity.Infrastructure.DbUpdateException),
                delegate { repositoryTest.AddUser(user); });
            Assert.That(ex.InnerException.InnerException.Message.Contains(
                "Cannot insert duplicate key row in object 'dbo.UserMaster' with unique index 'IX_UserMaster_UserName'. The duplicate key value is")
                );
            repositoryTest.RemoveUser(user.UserMasterId);
        }

        [Test]
        public void test_GetUserById()
        {
            var user = repositoryTest.GetUsers().FirstOrDefault();
            var foundUser = repositoryTest.Get(user.UserMasterId);
            Assert.IsTrue((foundUser != null) && (foundUser.UserMasterId == user.UserMasterId));
        }

        [Test]
        public void test_GetUserById_NotFound()
        {
            var foundUser = repositoryTest.Get(999);
            Assert.IsTrue(foundUser == null); 
        }

        [Test]
        public void test_GetUserByUserName()
        {
            var user = repositoryTest.GetUsers().FirstOrDefault();
            var foundUser = repositoryTest.Get(user.Username);
            Assert.IsTrue((foundUser != null) && (foundUser.UserMasterId == user.UserMasterId));
        }

        [Test]
        public void test_GetUserByUserNameAndPassword()
        {
            var user = repositoryTest.GetUsers().FirstOrDefault();
            var foundUser = repositoryTest.Get(user.Username, user.Password);
            Assert.IsTrue((foundUser != null) && (foundUser.UserMasterId == user.UserMasterId));
        }

        [Test]
        public void test_GetUserGraph()
        {
            var user = repositoryTest.GetUsers().FirstOrDefault();
            var foundUser = repositoryTest.GetUserGraph(user.Username, user.Password);
            Assert.IsTrue((foundUser != null) && (foundUser.UserMasterId == user.UserMasterId));
        }
        
        [Test]
        public void test_GetUsers()
        {
            IEnumerable<UserMaster> userMasters = repositoryTest.GetUsers();
            Assert.IsTrue(userMasters.Count<UserMaster>() >= 2);
        }

        [Test]
        public void test_update_user()
        {
            repositoryTest.AddUser(user);
            var newUser = repositoryTest.Get("gpuglisi");
            newUser.FirstName = "GPUpdated";
            newUser.LastName = "gpupdated";
            newUser.EmailAddress = "gpupdated@yahoo.com";
            newUser.Password = "gppassword";
            newUser.ModifiedBy = "test";

            var user2 = repositoryTest.UpdateUser(newUser);
            Assert.IsTrue(user2.FirstName == newUser.FirstName);
            Assert.IsTrue(user2.LastName == newUser.LastName);
            Assert.IsTrue(user2.EmailAddress == newUser.EmailAddress);
            Assert.IsTrue(user2.ModifiedBy == "test");
            Assert.IsTrue(user2.ModifiedOn.Value.Date == DateTime.Now.ToUniversalTime().Date);
            repositoryTest.RemoveUser(user.UserMasterId);
        }

        [Test]
        public void test_remove_users()
        {
            repositoryTest.AddUser(user);
            repositoryTest.RemoveUser(user.UserMasterId);
            Assert.IsTrue(repositoryTest.Get(user.UserMasterId) == null);
        }

        [Test]
        public void test_GetUserRolesById()
        {
            var user = repositoryTest.GetUsers().FirstOrDefault();
            var foundUserRoles = repositoryTest.GetRoles(user.UserMasterId);
            Assert.IsTrue((foundUserRoles != null) && (foundUserRoles.Count() > 0));
        }

        [Test]
        public void test_GetUserRolesByUsername()
        {
            var user = repositoryTest.GetUsers().FirstOrDefault();
            var foundUserRoles = repositoryTest.GetRoles(user.Username);
            Assert.IsTrue((foundUserRoles != null) && (foundUserRoles.Count() > 0));
        }

    }


    public class RepositoryFactoryTestClass
    {
        public RepositoryFactoryTestClass()
        {
            ObjectBase.Container.SatisfyImportsOnce(this);
        }
        public RepositoryFactoryTestClass(IDataRepositoryFactory dataRepositoryFactory)
        {
            _DataRepositoryFactory = dataRepositoryFactory;
        }

        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;

        public IEnumerable<UserMaster> GetUsers()
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.Get();
        }

        public UserMaster AddUser(UserMaster user)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            userMasterRepository.Add(user);
            return user;
        }

        public UserMaster UpdateUser(UserMaster user)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            userMasterRepository.Update(user);
            return user;
        }

        public void RemoveUser(int id)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            userMasterRepository.Remove(id);
        }

        public UserMaster Get(string userName)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.Get(userName);
        }

        public UserMaster Get(int entityId)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.Get(entityId);
        }

        public UserMaster GetUserGraph(string userName, string password)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.GetGraph(userName, password);
        }

        public UserMaster Get(string userName, string password)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.Get(userName, password);
        }

        public IEnumerable<RoleMaster> GetRoles(int userId)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.GetRoles(userId);
        }

        public IEnumerable<RoleMaster> GetRoles(string userName)
        {
            IUserMasterRepository userMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
            return userMasterRepository.GetRoles(userName);
        }

    }
}
