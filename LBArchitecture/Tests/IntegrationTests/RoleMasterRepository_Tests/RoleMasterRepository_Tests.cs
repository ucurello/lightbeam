﻿using Core.Common.Contracts;
using Core.Common.Core;
using LightBeam.Business.Bootstrapper;
using LightBeam.Data.Contracts.Repository_Interfaces;
using LightBeam.Data.Entities;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightBeam.Data.Tests.RoleMasterRepositoryTests
{
    [TestFixture]
    public class RoleMasterRepository_Tests
    {
        RepositoryFactoryTestClass repositoryTest;
        RoleMaster role;
        [TestFixtureSetUp]
        public void Initialize()
        {
            ObjectBase.Container = MEFLoader.Init();

            repositoryTest = new RepositoryFactoryTestClass();
            role = new RoleMaster()
            {
                Name = "TestRoleName",
                Description = "Test role description",
                CreatedBy = "test"
            };
        }

        [TestFixtureTearDown]
        public void CleanUp()
        {
        }


        [Test]
        public void AddTest()
        {
            RoleMaster addedRole = repositoryTest.Add(role);
            RoleMaster retrievedRole = repositoryTest.Get(addedRole.EntityId);
            Assert.IsNotNull(retrievedRole);
            Assert.IsTrue(retrievedRole.EntityId == addedRole.EntityId);
            repositoryTest.Remove(retrievedRole.EntityId);
        }

        [Test]
        public void RemoveByRoleEntityTest()
        {
            RoleMaster addedRole = repositoryTest.Add(role);
            repositoryTest.Remove(addedRole);
            addedRole = repositoryTest.Get(addedRole.EntityId);
            Assert.IsNull(addedRole);
        }

        [Test]
        public void RemoveRoleById()
        {
            RoleMaster addedRole = repositoryTest.Add(role);
            repositoryTest.Remove(addedRole.EntityId);
            addedRole = repositoryTest.Get(addedRole.EntityId);
            Assert.IsNull(addedRole);
        }

        [Test]
        public void Update()
        {
            RoleMaster addedRole = repositoryTest.Add(role);
            addedRole.Description = "Updated role description";
            addedRole.ModifiedBy = "test";
            repositoryTest.Update(addedRole);
            RoleMaster updatedRole = repositoryTest.Get(addedRole.EntityId);
            Assert.IsNotNull(updatedRole);
            Assert.IsTrue(addedRole.Description == updatedRole.Description);
            Assert.IsTrue(updatedRole.ModifiedBy == "test");
            Assert.IsTrue(updatedRole.ModifiedOn.Value.Date == DateTime.Now.ToUniversalTime().Date);
            repositoryTest.Remove(addedRole.EntityId);
        }

        [Test]
        public void GetRoles()
        {
            RoleMaster addedRole = repositoryTest.Add(role);
            var roles = repositoryTest.Get();
            Assert.IsNotNull(roles);
            Assert.IsTrue(roles.Count() > 0);
            repositoryTest.Remove(addedRole.EntityId);
        }
        
        [Test]
        public void GetById()
        {
            RoleMaster addedRole = repositoryTest.Add(role);
            RoleMaster retrievedRole = repositoryTest.Get(addedRole.EntityId);
            Assert.IsNotNull(retrievedRole);
            repositoryTest.Remove(addedRole.EntityId);
        }
    }


    public class RepositoryFactoryTestClass 
    {
        public RepositoryFactoryTestClass()
        {
            ObjectBase.Container.SatisfyImportsOnce(this);
        }
        public RepositoryFactoryTestClass(IDataRepositoryFactory dataRepositoryFactory)
        {
            _DataRepositoryFactory = dataRepositoryFactory;
        }

        [Import]
        IDataRepositoryFactory _DataRepositoryFactory;

        public Entities.RoleMaster Add(RoleMaster role)
        {
            IRoleMasterRepository roleMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();   
            return roleMasterRepository.Add(role);
        }

        public void Remove(RoleMaster role)
        {
            IRoleMasterRepository roleMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();   
            roleMasterRepository.Remove(role);
        }

        public void Remove(int id)
        {
            IRoleMasterRepository roleMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();   
            roleMasterRepository.Remove(id);
        }

        public Entities.RoleMaster Update(RoleMaster role)
        {
            IRoleMasterRepository roleMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();   
            return roleMasterRepository.Update(role);
        }

        public IEnumerable<RoleMaster> Get()
        {
            IRoleMasterRepository roleMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();   
            return roleMasterRepository.Get();
        }

        public Entities.RoleMaster Get(int id)
        {
            IRoleMasterRepository roleMasterRepository =
                _DataRepositoryFactory.GetDataRepository<IRoleMasterRepository>();   
            return roleMasterRepository.Get(id);
        }
    }

}
