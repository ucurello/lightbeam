﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AutoMapper;
using Moq;
using LightBeam.Business.Contracts.Security;
using LightBeam.Data.Entities;
using Core.Common.Contracts;
using LightBeam.Data.Contracts.Repository_Interfaces;
using Core.Common.Core;

using RoleMaster = LightBeam.Business.Entities.Security.RoleMaster;
using UserMaster = LightBeam.Business.Entities.Security.UserMaster;
using System.ComponentModel.Composition;


namespace LightBeam.Business.Managers.Tests.SecurityManager_Tests
{
    [TestFixture]
    public class SecurityManager_Tests
    {
        LightBeam.Data.Entities.UserMaster user;
        [TestFixtureSetUp]
        public void Initialize()
        {
        }

        [TestFixtureTearDown]
        public void CleanUp()
        {

        }

        [Test]
        public void CreateUser()
        {
            // Create some mappings
            Mapper.CreateMap<Data.Entities.UserMaster, UserMaster>();
            Mapper.CreateMap<UserMaster, Data.Entities.UserMaster>();

            // Setup the Mock for the data repository first
            user = new LightBeam.Data.Entities.UserMaster()
                {
                        FirstName = "Giuseppe",
                        LastName = "Puglisi",
                        Username = "gpuglisi",
                        EmailAddress = "gpuglisi@yahoo.com",
                        Password = "gppassword",
                        CreatedBy = "ucurello",
                        CreatedOn = DateTime.Now.ToUniversalTime()
                };
            
            Mock<IUserMasterRepository> mockUserMasterRepository = new Mock<IUserMasterRepository>();
            mockUserMasterRepository.Setup(obj => obj.Add(It.IsAny<LightBeam.Data.Entities.UserMaster>())).Returns(user);
            Mock<IDataRepositoryFactory> mockRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockRepositoryFactory.Setup(obj => obj.GetDataRepository<IUserMasterRepository>()).Returns(mockUserMasterRepository.Object);

            // Instantiate the security service and call the GetUsers method
            var securityService = new LightBeam.Business.Managers.SecurityService(mockRepositoryFactory.Object);
            var addedUser = securityService.CreateUser("ucurello", Mapper.Map<LightBeam.Data.Entities.UserMaster, UserMaster>(user));
            Assert.IsNotNull(addedUser);
            Assert.IsTrue(addedUser.CreatedBy == "ucurello");
        }

        [Test]
        public void GetRole()
        {
            // Setup the Mock for the data repository first
            var role = new LightBeam.Data.Entities.RoleMaster()
                {
                    RoleMasterId = 1,
                    Name = "TestRole",
                    Description = "Test description",
                    CreatedOn = DateTime.Now.ToUniversalTime(),
                    CreatedBy = "ucurello"
                };

            Mock<IRoleMasterRepository> mockRoleMasterRepository = new Mock<IRoleMasterRepository>();
            mockRoleMasterRepository.Setup(obj => obj.Get(It.IsAny<int>())).Returns(role);
            Mock<IDataRepositoryFactory> mockRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockRepositoryFactory.Setup(obj => obj.GetDataRepository<IRoleMasterRepository>()).Returns(mockRoleMasterRepository.Object);

            // Instantiate the security service and call the GetUsers method
            var securityService = new LightBeam.Business.Managers.SecurityService(mockRepositoryFactory.Object);
            var retrievedRole = securityService.GetRole(role.EntityId);
            Assert.IsNotNull(retrievedRole);
            Assert.IsTrue(retrievedRole.RoleMasterId == role.RoleMasterId);
        }

        [Test]
        public void GetRoles()
        {
            // Setup the Mock for the data repository first
            var roles = new List<LightBeam.Data.Entities.RoleMaster>()
                {
                new LightBeam.Data.Entities.RoleMaster
                    {
                    RoleMasterId = 1,
                    Name = "TestRole",
                    Description = "Test description",
                    CreatedOn = DateTime.Now.ToUniversalTime(),
                    CreatedBy = "ucurello"
                    },
                new LightBeam.Data.Entities.RoleMaster 
                    {
                    RoleMasterId = 2,
                    Name = "SecondRole",
                    Description = "Second description",
                    CreatedOn = DateTime.Now.ToUniversalTime(),
                    CreatedBy = "ucurello"
                    }
                };

            Mock<IRoleMasterRepository> mockRoleMasterRepository = new Mock<IRoleMasterRepository>();
            mockRoleMasterRepository.Setup(obj => obj.Get()).Returns(roles);
            Mock<IDataRepositoryFactory> mockRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockRepositoryFactory.Setup(obj => obj.GetDataRepository<IRoleMasterRepository>()).Returns(mockRoleMasterRepository.Object);

            // Instantiate the security service and call the GetUsers method
            var securityService = new LightBeam.Business.Managers.SecurityService(mockRepositoryFactory.Object);
            var retrievedRoles = securityService.GetRoles();
            Assert.IsNotNull(retrievedRoles);
            Assert.IsTrue(retrievedRoles.FirstOrDefault().RoleMasterId == roles.FirstOrDefault().RoleMasterId);
        }

        [Test]
        public void GetUser(int userId)
        {
            throw new NotImplementedException();
        }

        [Test]
        public void GetUserByLogin(string username, string password)
        {
            throw new NotImplementedException();
        }

        [Test]
        public void GetUserRoles(int userId)
        {
            throw new NotImplementedException();
        }

        [Test]
        public void GetUserRoles(string username)
        {
            throw new NotImplementedException();
        }

        [Test]
        public void GetUsers()
        {
            // Setup the Mock for the data repository first
            var users = new List<LightBeam.Data.Entities.UserMaster>()
                {
                    new LightBeam.Data.Entities.UserMaster {
                        FirstName = "Giuseppe",
                        LastName = "Puglisi",
                        Username = "gpuglisi",
                        EmailAddress = "gpuglisi@yahoo.com",
                        Password = "gppassword",
                        CreatedBy = "ucurello"
                    },
                    new LightBeam.Data.Entities.UserMaster {
                        FirstName = "Claudio",
                        LastName = "Moretti",
                        Username = "cmoretti",
                        EmailAddress = "cmoretti@yahoo.com",
                        Password = "cmpassword",
                        CreatedBy = "ucurello"
                    }
                };

            Mock<IUserMasterRepository> mockUserMasterRepository = new Mock<IUserMasterRepository>();
            mockUserMasterRepository.Setup(obj => obj.Get()).Returns(users);
            Mock<IDataRepositoryFactory> mockRepositoryFactory = new Mock<IDataRepositoryFactory>();
            mockRepositoryFactory.Setup(obj => obj.GetDataRepository<IUserMasterRepository>()).Returns(mockUserMasterRepository.Object);

            // Instantiate the security service and call the GetUsers method
            var securityService = new LightBeam.Business.Managers.SecurityService(mockRepositoryFactory.Object);
            var retrievedUsers = securityService.GetUsers();
            Assert.IsNotNull(retrievedUsers);
            Assert.IsTrue(retrievedUsers.FirstOrDefault().UserMasterId == users.FirstOrDefault().UserMasterId);
        }

        [Test]
        public void UpdateUser(string modifiedBy, Entities.Security.UserMaster user)
        {
            throw new NotImplementedException();
        }
    }

    //public class TestService
    //{
    //    [Import]
    //    IDataRepositoryFactory _dataRepositoryFactory;
    //    public TestService()
    //    {
    //    }

    //    public TestService(IDataRepositoryFactory dataRepositoryFactory)
    //    {
    //        _dataRepositoryFactory = dataRepositoryFactory;
    //    }

    //    public void TestCreate(LightBeam.Data.Entities.UserMaster user)
    //    {
    //        // Instantiate the security service and call the GetUsers method
    //        var myUserRepo = _dataRepositoryFactory.GetDataRepository<IUserMasterRepository>();
    //        user = myUserRepo.Add(user);
    //    }
    //}


}
