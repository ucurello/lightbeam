﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Common.Contracts;
using LightBeam.Data.Entities;

namespace LightBeam.Data.Contracts.Repository_Interfaces
{
    public interface IUserMasterRepository : IDataRepository<UserMaster>
    {
        UserMaster Get(string username, string password);
        UserMaster Get(string username);
        IEnumerable<RoleMaster> GetRoles(string userName);
        IEnumerable<RoleMaster> GetRoles(int userId);
        UserMaster GetGraph(string username, string password);
    }
}
