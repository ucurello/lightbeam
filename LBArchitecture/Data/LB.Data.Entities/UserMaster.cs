using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class UserMaster : IIdentifiableEntity
    {
        public UserMaster()
        {
            this.UserContacts = new List<UserContact>();
            this.UserSecurities = new List<UserSecurity>();
            this.UserSecurityQuestions = new List<UserSecurityQuestion>();
            this.RoleMasters = new List<RoleMaster>();
        }

        public int UserMasterId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordStrength { get; set; }
        public string DefaultPage { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public virtual ICollection<UserContact> UserContacts { get; set; }
        public virtual ICollection<UserSecurity> UserSecurities { get; set; }
        public virtual ICollection<UserSecurityQuestion> UserSecurityQuestions { get; set; }
        public virtual ICollection<RoleMaster> RoleMasters { get; set; }

        #region IIdentifiable members
        public int EntityId
        {
            get { return UserMasterId; }
            set
            {
                UserMasterId = value;
            }
        }
        #endregion
    }
}
