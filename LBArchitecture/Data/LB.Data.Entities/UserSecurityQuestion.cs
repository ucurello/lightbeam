using System;
using System.Collections.Generic;

namespace LightBeam.Data.Entities
{
    public partial class UserSecurityQuestion
    {
        public int UserSecurityQuestionId { get; set; }
        public Nullable<int> UserMasterId { get; set; }
        public string SecurityQuestion { get; set; }
        public string Answer { get; set; }
        public Nullable<System.DateTime> CreateTimestamp { get; set; }
        public Nullable<System.DateTime> ModifyTimestamp { get; set; }
        public virtual UserMaster UserMaster { get; set; }
    }
}
