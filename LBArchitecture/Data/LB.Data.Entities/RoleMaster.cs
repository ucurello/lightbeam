using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class RoleMaster : IIdentifiableEntity
    {
        public RoleMaster()
        {
            this.RoleModuleItemPermissions = new List<RoleModuleItemPermission>();
            this.UserMasters = new List<UserMaster>();
        }

        public int RoleMasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public virtual ICollection<RoleModuleItemPermission> RoleModuleItemPermissions { get; set; }
        public virtual ICollection<UserMaster> UserMasters { get; set; }
        #region IIdentifiable members
        public int EntityId
        {
            get { return RoleMasterId; }
            set
            {
                RoleMasterId = value;
            }
        }
        #endregion
    }
}
