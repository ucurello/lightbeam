using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class ModuleMaster : IIdentifiableEntity
    {
        public ModuleMaster()
        {
            this.ModuleItems = new List<ModuleItem>();
        }

        public int ModuleMasterId { get; set; }
        public int AppMasterId { get; set; }
        public string ModuleName { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public virtual ApplicationMaster ApplicationMaster { get; set; }
        public virtual ICollection<ModuleItem> ModuleItems { get; set; }
        #region IIdentifiable members
        public int EntityId
        {
            get { return ModuleMasterId; }
            set
            {
                ModuleMasterId = value;
            }
        }
        #endregion
    }
}
