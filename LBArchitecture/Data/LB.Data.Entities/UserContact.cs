using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class UserContact : IIdentifiableEntity
    {
        public int UserContactId { get; set; }
        public Nullable<int> UserMasterId { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public string Password { get; set; }
        public string Notes { get; set; }
        public Nullable<System.DateTime> CreateTimestamp { get; set; }
        public Nullable<System.DateTime> ModifyTimestamp { get; set; }
        public virtual UserMaster UserMaster { get; set; }

        #region IIdentifiable members
        public int EntityId
        {
            get { return UserContactId; }
            set
            {
                UserContactId = value;
            }
        }
        #endregion

    }
}
