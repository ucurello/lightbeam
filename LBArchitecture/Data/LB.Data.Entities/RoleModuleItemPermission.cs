using System;
using System.Collections.Generic;

namespace LightBeam.Data.Entities
{
    public partial class RoleModuleItemPermission
    {
        public int RoleMasterId { get; set; }
        public int ModuleItemId { get; set; }
        public int PermissionMasterId { get; set; }
        public virtual ModuleItem ModuleItem { get; set; }
        public virtual PermissionMaster PermissionMaster { get; set; }
        public virtual RoleMaster RoleMaster { get; set; }
    }
}
