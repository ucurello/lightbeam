using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class UserSecurity : IIdentifiableEntity
    {
        public int UserSecurityID { get; set; }
        public int UserMasterId { get; set; }
        public Nullable<System.DateTime> TimeLastAccessed { get; set; }
        public Nullable<System.DateTime> TimePasswordReset { get; set; }
        public Nullable<bool> AdminPasswordReset { get; set; }
        public Nullable<bool> ForceChangePassword { get; set; }
        public byte[] SecurityImage { get; set; }
        public Nullable<System.DateTime> CreateTimestamp { get; set; }
        public Nullable<System.DateTime> ModifyTimestamp { get; set; }
        public virtual UserMaster UserMaster { get; set; }
        #region IIdentifiable members
        public int EntityId
        {
            get { return UserSecurityID; }
            set
            {
                UserSecurityID = value;
            }
        }
        #endregion
    }
}
