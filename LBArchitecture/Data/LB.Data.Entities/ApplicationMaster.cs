using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class ApplicationMaster : IIdentifiableEntity
    {
        public ApplicationMaster()
        {
            this.ModuleMasters = new List<ModuleMaster>();
        }

        public int AppMasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public virtual ICollection<ModuleMaster> ModuleMasters { get; set; }
        #region IIdentifiable members
        public int EntityId
        {
            get { return AppMasterId; }
            set { AppMasterId = value; }
        }
        #endregion
    }
}
