using System;
using System.Collections.Generic;
using Core.Common.Contracts;

namespace LightBeam.Data.Entities
{
    public partial class ModuleItem : IIdentifiableEntity
    {
        public ModuleItem()
        {
            this.RoleModuleItemPermissions = new List<RoleModuleItemPermission>();
        }

        public int ModuleItemId { get; set; }
        public int ModuleMasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public virtual ICollection<RoleModuleItemPermission> RoleModuleItemPermissions { get; set; }
        public virtual ModuleMaster ModuleMaster { get; set; }

        #region IIdentifiable members
        public int EntityId
        {
            get { return ModuleItemId; }
            set { ModuleItemId = value; }
        }
        #endregion

    }
}
