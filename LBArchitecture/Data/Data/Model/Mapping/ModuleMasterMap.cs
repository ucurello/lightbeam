using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class ModuleMasterMap : EntityTypeConfiguration<ModuleMaster>
    {
        public ModuleMasterMap()
        {
            // Primary Key
            this.HasKey(t => t.ModuleMasterId);

            // Properties
            this.Property(t => t.ModuleMasterId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.ModuleName)
                .HasMaxLength(255);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            this.Property(t => t.CreatedBy)
                .HasMaxLength(50);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("ModuleMaster");
            this.Property(t => t.ModuleMasterId).HasColumnName("ModuleMasterId");
            this.Property(t => t.AppMasterId).HasColumnName("AppMasterId");
            this.Property(t => t.ModuleName).HasColumnName("ModuleName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");

            // Relationships
            this.HasRequired(t => t.ApplicationMaster)
                .WithMany(t => t.ModuleMasters)
                .HasForeignKey(d => d.AppMasterId);

            this.Ignore(c => c.EntityId);
        }
    }
}
