using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class RoleMasterMap : EntityTypeConfiguration<RoleMaster>
    {
        public RoleMasterMap()
        {
            // Primary Key
            this.HasKey(t => t.RoleMasterId);

            // Properties
            this.Property(t => t.RoleMasterId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Name)
                .HasMaxLength(255);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            this.Property(t => t.CreatedBy)
                .HasMaxLength(50);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("RoleMaster");
            this.Property(t => t.RoleMasterId).HasColumnName("RoleMasterId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");

            // Relationships
            this.HasMany(t => t.UserMasters)
                .WithMany(t => t.RoleMasters)
                .Map(m =>
                    {
                        m.ToTable("UserRole");
                        m.MapLeftKey("RoleMasterId");
                        m.MapRightKey("UserMasterId");
                    });

            this.Ignore(c => c.EntityId);
        }
    }
}
