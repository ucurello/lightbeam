using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class UserSecurityQuestionMap : EntityTypeConfiguration<UserSecurityQuestion>
    {
        public UserSecurityQuestionMap()
        {
            // Primary Key
            this.HasKey(t => t.UserSecurityQuestionId);

            // Properties
            this.Property(t => t.UserSecurityQuestionId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            // Table & Column Mappings
            this.ToTable("UserSecurityQuestion");
            this.Property(t => t.UserSecurityQuestionId).HasColumnName("UserSecurityQuestionId");
            this.Property(t => t.UserMasterId).HasColumnName("UserMasterId");
            this.Property(t => t.SecurityQuestion).HasColumnName("SecurityQuestion");
            this.Property(t => t.Answer).HasColumnName("Answer");
            this.Property(t => t.CreateTimestamp).HasColumnName("CreateTimestamp");
            this.Property(t => t.ModifyTimestamp).HasColumnName("ModifyTimestamp");

            // Relationships
            this.HasOptional(t => t.UserMaster)
                .WithMany(t => t.UserSecurityQuestions)
                .HasForeignKey(d => d.UserMasterId);

        }
    }
}
