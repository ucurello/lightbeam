using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class UserContactMap : EntityTypeConfiguration<UserContact>
    {
        public UserContactMap()
        {
            // Primary Key
            this.HasKey(t => t.UserContactId);

            // Properties
            this.Property(t => t.UserContactId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Type)
                .HasMaxLength(50);

            this.Property(t => t.Value)
                .HasMaxLength(50);

            this.Property(t => t.Description)
                .HasMaxLength(50);

            this.Property(t => t.Password)
                .HasMaxLength(128);

            // Table & Column Mappings
            this.ToTable("UserContact");
            this.Property(t => t.UserContactId).HasColumnName("UserContactId");
            this.Property(t => t.UserMasterId).HasColumnName("UserMasterId");
            this.Property(t => t.Type).HasColumnName("Type");
            this.Property(t => t.Value).HasColumnName("Value");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.Notes).HasColumnName("Notes");
            this.Property(t => t.CreateTimestamp).HasColumnName("CreateTimestamp");
            this.Property(t => t.ModifyTimestamp).HasColumnName("ModifyTimestamp");

            // Relationships
            this.HasOptional(t => t.UserMaster)
                .WithMany(t => t.UserContacts)
                .HasForeignKey(d => d.UserMasterId);

            this.Ignore(c => c.EntityId);
        }
    }
}
