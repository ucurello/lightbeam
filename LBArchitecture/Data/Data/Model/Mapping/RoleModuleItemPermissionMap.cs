using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class RoleModuleItemPermissionMap : EntityTypeConfiguration<RoleModuleItemPermission>
    {
        public RoleModuleItemPermissionMap()
        {
            // Primary Key
            this.HasKey(t => new { t.RoleMasterId, t.ModuleItemId, t.PermissionMasterId });

            // Properties
            this.Property(t => t.RoleMasterId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.ModuleItemId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.PermissionMasterId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("RoleModuleItemPermission");
            this.Property(t => t.RoleMasterId).HasColumnName("RoleMasterId");
            this.Property(t => t.ModuleItemId).HasColumnName("ModuleItemId");
            this.Property(t => t.PermissionMasterId).HasColumnName("PermissionMasterId");

            // Relationships
            this.HasRequired(t => t.ModuleItem)
                .WithMany(t => t.RoleModuleItemPermissions)
                .HasForeignKey(d => d.ModuleItemId);
            this.HasRequired(t => t.PermissionMaster)
                .WithMany(t => t.RoleModuleItemPermissions)
                .HasForeignKey(d => d.PermissionMasterId);
            this.HasRequired(t => t.RoleMaster)
                .WithMany(t => t.RoleModuleItemPermissions)
                .HasForeignKey(d => d.RoleMasterId);

        }
    }
}
