using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class UserMasterMap : EntityTypeConfiguration<UserMaster>
    {
        public UserMasterMap()
        {
            // Primary Key
            this.HasKey(t => t.UserMasterId);

            // Properties
            this.Property(t => t.UserMasterId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.FirstName)
                .HasMaxLength(50);

            this.Property(t => t.LastName)
                .HasMaxLength(50);

            this.Property(t => t.EmailAddress)
                .HasMaxLength(100);

            this.Property(t => t.Username)
                .HasMaxLength(50);

            this.Property(t => t.Password)
                .HasMaxLength(100);

            this.Property(t => t.PasswordSalt)
                .HasMaxLength(10);

            this.Property(t => t.PasswordStrength)
                .HasMaxLength(20);

            this.Property(t => t.DefaultPage)
                .HasMaxLength(1000);

            this.Property(t => t.CreatedBy)
                .HasMaxLength(50);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("UserMaster");
            this.Property(t => t.UserMasterId).HasColumnName("UserMasterId");
            this.Property(t => t.FirstName).HasColumnName("FirstName");
            this.Property(t => t.LastName).HasColumnName("LastName");
            this.Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            this.Property(t => t.Username).HasColumnName("Username");
            this.Property(t => t.Password).HasColumnName("Password");
            this.Property(t => t.PasswordSalt).HasColumnName("PasswordSalt");
            this.Property(t => t.PasswordStrength).HasColumnName("PasswordStrength");
            this.Property(t => t.DefaultPage).HasColumnName("DefaultPage");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            this.Ignore(c => c.EntityId);
        }
    }
}
