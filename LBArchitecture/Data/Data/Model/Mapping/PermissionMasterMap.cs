using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class PermissionMasterMap : EntityTypeConfiguration<PermissionMaster>
    {
        public PermissionMasterMap()
        {
            // Primary Key
            this.HasKey(t => t.PermissionMasterId);

            // Properties
            this.Property(t => t.PermissionMasterId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Name)
                .HasMaxLength(255);

            this.Property(t => t.FriendlyName)
                .HasMaxLength(255);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            this.Property(t => t.CreatedBy)
                .HasMaxLength(50);

            this.Property(t => t.ModifiedBy)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("PermissionMaster");
            this.Property(t => t.PermissionMasterId).HasColumnName("PermissionMasterId");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.FriendlyName).HasColumnName("FriendlyName");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CreatedOn).HasColumnName("CreatedOn");
            this.Property(t => t.ModifiedOn).HasColumnName("ModifiedOn");
            this.Property(t => t.CreatedBy).HasColumnName("CreatedBy");
            this.Property(t => t.ModifiedBy).HasColumnName("ModifiedBy");
            
            this.Ignore(c => c.EntityId);
        }
    }
}
