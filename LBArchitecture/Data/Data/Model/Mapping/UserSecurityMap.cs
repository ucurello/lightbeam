using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace LightBeam.Data.Entities.Mapping
{
    public class UserSecurityMap : EntityTypeConfiguration<UserSecurity>
    {
        public UserSecurityMap()
        {
            // Primary Key
            this.HasKey(t => t.UserSecurityID);

            // Properties
            // Table & Column Mappings
            this.ToTable("UserSecurity");
            this.Property(t => t.UserSecurityID).HasColumnName("UserSecurityID").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.UserMasterId).HasColumnName("UserMasterId");
            this.Property(t => t.TimeLastAccessed).HasColumnName("TimeLastAccessed");
            this.Property(t => t.TimePasswordReset).HasColumnName("TimePasswordReset");
            this.Property(t => t.AdminPasswordReset).HasColumnName("AdminPasswordReset");
            this.Property(t => t.ForceChangePassword).HasColumnName("ForceChangePassword");
            this.Property(t => t.SecurityImage).HasColumnName("SecurityImage");
            this.Property(t => t.CreateTimestamp).HasColumnName("CreateTimestamp");
            this.Property(t => t.ModifyTimestamp).HasColumnName("ModifyTimestamp");

            // Relationships
            this.HasRequired(t => t.UserMaster)
                .WithMany(t => t.UserSecurities)
                .HasForeignKey(d => d.UserMasterId);

            this.Ignore(c => c.EntityId);
        }
    }
}
