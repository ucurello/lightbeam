using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System;
using System.Collections.Generic;

using System.Data.Entity.ModelConfiguration.Conventions;
using Core.Common.Contracts;
using LightBeam.Data.Entities;
using LightBeam.Data.Entities.Mapping;

namespace LightBeam.Data.Model
{

    public class DatabaseContextInitializer : DropCreateDatabaseIfModelChanges<LBContext>
    {
        protected override void Seed(LBContext dbContext)
        {

            CreateIndex(dbContext, "UserName", "UserMaster", true);

            var app = new ApplicationMaster() { Name = "LB Portal", Description = "LB Portal", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" };
            var module = new ModuleMaster() { ModuleName = "Admin", Description = "Admin", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" };
            app.ModuleMasters.Add(module);
            dbContext.ApplicationMasterSet.Add(app);
            var defaultRoles = new List<RoleMaster>();
            defaultRoles.Add(new RoleMaster() { Name = "Admin", Description = "Admin", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultRoles.Add(new RoleMaster() { Name = "Manager", Description = "Manager", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultRoles.Add(new RoleMaster() { Name = "Care Manager", Description = "Care Manager", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultRoles.Add(new RoleMaster() { Name = "Executive Manager", Description = "Executive Manager", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            foreach (RoleMaster role in defaultRoles)
                dbContext.RoleMasterSet.Add(role);
            var defaultPermission = new List<PermissionMaster>();
            defaultPermission.Add(new PermissionMaster() { Name = "Read", FriendlyName = "Read", Description = "Read", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultPermission.Add(new PermissionMaster() { Name = "Update", FriendlyName = "Update", Description = "Update", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultPermission.Add(new PermissionMaster() { Name = "Delete", FriendlyName = "Delete", Description = "Delete", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultPermission.Add(new PermissionMaster() { Name = "Reset", FriendlyName = "Reset", Description = "Reset", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            defaultPermission.Add(new PermissionMaster() { Name = "Create", FriendlyName = "Creat", Description = "Create", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto" });
            foreach (PermissionMaster perm in defaultPermission)
                dbContext.PermissionMasterSet.Add(perm);

            var defaultModuleItem = new List<ModuleItem>();
            defaultModuleItem.Add(new ModuleItem() { Name = "Role Management", Description = "Role Management", CreatedBy = "auto", CreatedOn = DateTime.Now.ToUniversalTime(), ModuleMaster = module });
            defaultModuleItem.Add(new ModuleItem() { Name = "User Management", Description = "User Management", CreatedBy = "auto", CreatedOn = DateTime.Now.ToUniversalTime(), ModuleMaster = module });
            defaultModuleItem.Add(new ModuleItem() { Name = "Other Management", Description = "Other Management", CreatedBy = "auto", CreatedOn = DateTime.Now.ToUniversalTime(), ModuleMaster = module });
            foreach (ModuleItem mi in defaultModuleItem)
                dbContext.ModuleItemSet.Add(mi);

            var defaultRoleModuleItem = new List<RoleModuleItemPermission>();
            defaultRoleModuleItem.Add(new RoleModuleItemPermission() { RoleMaster = defaultRoles[1], ModuleItem = defaultModuleItem[0], PermissionMaster = defaultPermission[0] });
            defaultRoleModuleItem.Add(new RoleModuleItemPermission() { RoleMaster = defaultRoles[1], ModuleItem = defaultModuleItem[0], PermissionMaster = defaultPermission[1] });
            defaultRoleModuleItem.Add(new RoleModuleItemPermission() { RoleMaster = defaultRoles[1], ModuleItem = defaultModuleItem[0], PermissionMaster = defaultPermission[2] });
            defaultRoleModuleItem.Add(new RoleModuleItemPermission() { RoleMaster = defaultRoles[1], ModuleItem = defaultModuleItem[0], PermissionMaster = defaultPermission[3] });
            defaultRoleModuleItem.Add(new RoleModuleItemPermission() { RoleMaster = defaultRoles[1], ModuleItem = defaultModuleItem[0], PermissionMaster = defaultPermission[4] });
            foreach (RoleModuleItemPermission rip in defaultRoleModuleItem)
                dbContext.RoleModuleItemPermissionSet.Add(rip);

            var defaultUsers = new List<UserMaster>();
            defaultUsers.Add(new UserMaster() { FirstName = "Ugo", LastName = "Curello", Username = "ucurello", EmailAddress = "ugoc@yahoo.com", DefaultPage = "http://localhost:10000/Communication", Password = "letmein", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto", RoleMasters = defaultRoles });
            defaultUsers.Add(new UserMaster() { FirstName = "Nikhil", LastName = "Oza", Username = "noza", EmailAddress = "nikhil.oza@gmail.com", DefaultPage = "http://localhost:10000/Communication", Password = "letmein", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto", RoleMasters = defaultRoles });
            defaultUsers.Add(new UserMaster() { FirstName = "Jibu", LastName = "George", Username = "jgeorge", EmailAddress = "jgeorge@lightbeamhealth.com", DefaultPage = "http://localhost:10000/Communication", Password = "letmein", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto", RoleMasters = defaultRoles });
            defaultUsers.Add(new UserMaster() { FirstName = "Robert", LastName = "Nary", Username = "bnary", EmailAddress = "bnary@lightbeamhealth.com", DefaultPage = "http://localhost:10000/Communication", Password = "letmein", CreatedOn = DateTime.Now.ToUniversalTime(), CreatedBy = "auto", RoleMasters = defaultRoles });
            foreach (UserMaster user in defaultUsers)
                dbContext.UserMasterSet.Add(user);

            dbContext.SaveChanges();

            base.Seed(dbContext);
        }

        private void CreateIndex(LBContext context, string field, string table, bool unique = false)
        {
            context.Database.ExecuteSqlCommand(String.Format("CREATE {0}NONCLUSTERED INDEX IX_{1}_{2} ON {1} ({3})",
                unique ? "UNIQUE " : "",
                table,
                field.Replace(",", "_"),
                field));
        } 

    }

    public partial class LBContext : DbContext
    {
        static LBContext()
        {
            Database.SetInitializer<LBContext>(new DatabaseContextInitializer());
        }

        public LBContext()
            : base("Name=LB")
        {
        }

        public DbSet<ApplicationMaster> ApplicationMasterSet { get; set; }
        public DbSet<ModuleItem> ModuleItemSet { get; set; }
        public DbSet<ModuleMaster> ModuleMasterSet { get; set; }
        public DbSet<PermissionMaster> PermissionMasterSet { get; set; }
        public DbSet<RoleMaster> RoleMasterSet { get; set; }
        public DbSet<RoleModuleItemPermission> RoleModuleItemPermissionSet { get; set; }
        public DbSet<UserContact> UserContactSet { get; set; }
        public DbSet<UserMaster> UserMasterSet { get; set; }
        public DbSet<UserSecurity> UserSecuritySet { get; set; }
        public DbSet<UserSecurityQuestion> UserSecurityQuestionSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Ignore<IIdentifiableEntity>();

            modelBuilder.Configurations.Add(new ApplicationMasterMap());
            modelBuilder.Configurations.Add(new ModuleItemMap());
            modelBuilder.Configurations.Add(new ModuleMasterMap());
            modelBuilder.Configurations.Add(new PermissionMasterMap());
            modelBuilder.Configurations.Add(new RoleMasterMap());
            modelBuilder.Configurations.Add(new RoleModuleItemPermissionMap());
            modelBuilder.Configurations.Add(new sysdiagramMap());
            modelBuilder.Configurations.Add(new UserContactMap());
            modelBuilder.Configurations.Add(new UserMasterMap());
            modelBuilder.Configurations.Add(new UserSecurityMap());
            modelBuilder.Configurations.Add(new UserSecurityQuestionMap());
        }
    }
}
