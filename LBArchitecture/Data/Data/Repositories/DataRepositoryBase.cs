﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Common.Contracts;
using Core.Common.Data;
using LightBeam.Data.Model;

namespace LightBeam.Data
{
    public abstract class DataRepositoryBase<T> : Core.Common.Data.DataRepositoryBase<T, LBContext>
        where T : class, IIdentifiableEntity, new()
    {
    }
}
