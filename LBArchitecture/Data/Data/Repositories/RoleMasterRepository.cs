﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Common.Data;
using LightBeam.Data.Contracts.Repository_Interfaces;
using LightBeam.Data.Entities;
using LightBeam.Data.Model;

namespace LightBeam.Data.Repositories
{
    [Export(typeof(IRoleMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class RoleMasterRepository : DataRepositoryBase<RoleMaster>, IRoleMasterRepository
    {

        protected override RoleMaster AddEntity(LBContext entityContext, RoleMaster entity)
        {
            entity.CreatedOn = DateTime.Now.ToUniversalTime();
            return entityContext.RoleMasterSet.Add(entity);
        }

        protected override RoleMaster UpdateEntity(LBContext entityContext, RoleMaster entity)
        {
            entity.ModifiedOn = DateTime.Now.ToUniversalTime();
            return (from e in entityContext.RoleMasterSet
                    where e.RoleMasterId == entity.RoleMasterId
                    select e).FirstOrDefault();
        }

        protected override IEnumerable<RoleMaster> GetEntities(LBContext entityContext)
        {
            return from e in entityContext.RoleMasterSet
                   select e;
        }

        protected override RoleMaster GetEntity(LBContext entityContext, int id)
        {
            var query = (from e in entityContext.RoleMasterSet
                         where e.RoleMasterId == id
                         select e);
            var results = query.FirstOrDefault();

            return results;
        }

    }
}
