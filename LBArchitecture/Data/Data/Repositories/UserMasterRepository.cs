﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Core.Common.Data;
using LightBeam.Data.Contracts.Repository_Interfaces;
using LightBeam.Data.Entities;
using LightBeam.Data.Model;

namespace LightBeam.Data.Repositories
{
    [Export(typeof(IUserMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserMasterRepository : DataRepositoryBase<UserMaster>, IUserMasterRepository
    {

        protected override UserMaster AddEntity(LBContext entityContext, UserMaster entity)
        {
            entity.CreatedOn = DateTime.Now.ToUniversalTime();
            return entityContext.UserMasterSet.Add(entity);
        }

        protected override UserMaster UpdateEntity(LBContext entityContext, UserMaster entity)
        {
            entity.ModifiedOn = DateTime.Now.ToUniversalTime();
            return (from e in entityContext.UserMasterSet
                    where e.UserMasterId == entity.UserMasterId
                    select e).FirstOrDefault();
        }

        protected override IEnumerable<UserMaster> GetEntities(LBContext entityContext)
        {
            return from e in entityContext.UserMasterSet
                   select e;
        }

        protected override UserMaster GetEntity(LBContext entityContext, int id)
        {
            var query = (from e in entityContext.UserMasterSet
                         where e.UserMasterId == id
                         select e);
            var results = query.FirstOrDefault();
            return results;
        }

        public UserMaster Get(string userName, string password)
        {
            using (var entityContext = new LBContext())
            {
                var user = entityContext.UserMasterSet.FirstOrDefault(x => x.Username == userName && x.Password == password);
                return user;                
            }
        }

        public UserMaster Get(string userName)
        {
            using (var entityContext = new LBContext())
            {
                var user = entityContext.UserMasterSet.FirstOrDefault(x => x.Username == userName);
                return user;
            }
        }

        public IEnumerable<RoleMaster> GetRoles(int userId)
        {
            using (var entityContext = new LBContext())
            {
                UserMaster user = GetEntity(entityContext, userId);
                if (user != null)
                {
                    return user.RoleMasters.ToArray();
                }
                return null;                    
            }
        }

        public IEnumerable<RoleMaster> GetRoles(string userName)
        {
            using (var entityContext = new LBContext())
            {
                var firstOrDefault = entityContext.UserMasterSet.FirstOrDefault(x => x.Username == userName);
                if (firstOrDefault != null)
                {
                    return firstOrDefault.RoleMasters.ToArray();
                }
                return null;
            }
        }

        public UserMaster GetGraph(string userName, string password)
        {
            using (var entityContext = new LBContext())
            {
                var user = entityContext.UserMasterSet
                            .Where(x => x.Username == userName && x.Password == password)
                            .Include(x => x.RoleMasters.Select(s => s.RoleModuleItemPermissions.Select(p => p.PermissionMaster)))
                            .Include(x => x.UserSecurities)
                            .Include(x => x.UserSecurityQuestions)
                            .FirstOrDefault();
                return user;
            }
        }

    }
}
