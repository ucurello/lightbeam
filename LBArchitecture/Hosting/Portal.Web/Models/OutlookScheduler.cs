using System.Collections;

namespace LightBeam.Portal.Web.Models {
    public class OutlookScheduler {
        public IEnumerable Appointments {
            get {
                return OutlookDataProvider.GetAppointments();
            }
        }

        public IEnumerable Resources {
            get {
                return OutlookDataProvider.GetResources();
            }
        }
    }
}