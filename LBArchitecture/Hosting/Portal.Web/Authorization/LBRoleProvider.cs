﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Security;
using LightBeam.Business.Entities.Security;

namespace LightBeam.Portal.Web.Authorization
{
    public class LBRoleProvider : RoleProvider
    {

        public override void AddUsersToRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override string ApplicationName
        {
            get { return "LB"; }
            set { value = "LB"; }
        }

        public override void CreateRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteRole(string roleName, bool throwOnPopulatedRole)
        {
            throw new NotImplementedException();
        }

        public override string[] FindUsersInRole(string roleName, string usernameToMatch)
        {
            throw new NotImplementedException();
        }

        public override string[] GetAllRoles()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["webAPIURL"].ToString());
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string request = string.Format("api/role");
            var response = client.GetAsync(request).Result;
            IEnumerable<RoleMaster> roles = null;
            if (response.IsSuccessStatusCode)
            {
                roles = response.Content.ReadAsAsync<IEnumerable<RoleMaster>>().Result;
                var roleNames = roles.Select(x => x.Name).ToArray();
                return roleNames;
            }
            return null;
            
        }

        public override string[] GetRolesForUser(string username)
        {
            
            var cacheKey = string.Format("UserRoles_{0}", username);
            //if (HttpRuntime.Cache[cacheKey] != null) return (string[])HttpRuntime.Cache[cacheKey];

            List<string> userRoles = new List<string>();

            var client = new HttpClient();
            client.BaseAddress = new Uri(System.Configuration.ConfigurationManager.AppSettings["webAPIURL"].ToString());
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            string request = string.Format("api/role?userName={0}", username);
            var response = client.GetAsync(request).Result;
            IEnumerable<RoleMaster> roles = null;
            if (response.IsSuccessStatusCode)
            {
                roles = response.Content.ReadAsAsync<IEnumerable<RoleMaster>>().Result;
                userRoles = roles.Select(x => x.Name).ToList();
            }
            HttpRuntime.Cache.Insert(cacheKey, userRoles.ToArray(), null);


            try
            {
                System.Security.Principal.GenericIdentity identity = (System.Security.Principal.GenericIdentity)HttpContext.Current.User.Identity;
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(identity, userRoles.ToArray());
            }
            catch
            {
                FormsIdentity identity = (FormsIdentity)HttpContext.Current.User.Identity;
                HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(identity, userRoles.ToArray());
            }
            

            return userRoles.ToArray();
        }

        public override string[] GetUsersInRole(string roleName)
        {
            throw new NotImplementedException();
        }

        public override bool IsUserInRole(string username, string roleName)
        {
            throw new NotImplementedException();
        }

        public override void RemoveUsersFromRoles(string[] usernames, string[] roleNames)
        {
            throw new NotImplementedException();
        }

        public override bool RoleExists(string roleName)
        {
            throw new NotImplementedException();
        }
    }
}