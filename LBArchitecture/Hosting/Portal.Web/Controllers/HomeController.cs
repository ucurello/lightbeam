using System.Web.Mvc;
using LightBeam.Portal.Web.Models;

namespace LightBeam.Portal.Web.Controllers {
    public class HomeController : Controller {
        [Authorize(Roles="Admin")]
        public ActionResult Index() {
            return View(OutlookDataProvider.GetMessages());
        }

        public ActionResult GridViewPartialView() {
            return PartialView("GridViewPartialView", OutlookDataProvider.GetMessages());
        }
    }
}