﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using RoleMaster = LightBeam.Business.Entities.Security.RoleMaster;
using Core.Common.Core;
using Core.Common.Contracts;
using System.ComponentModel.Composition;
using LightBeam.Business.Contracts.Security;

namespace LightBeam.SecurityAPI.Controllers
{
    public class RoleController : ApiController
    {
        public RoleController()
        {
            if (ObjectBase.Container != null)
                ObjectBase.Container.SatisfyImportsOnce(this);
        }

        public RoleController(IBusinessManagerFactory managerFactory) : this()
        {
            _managerFactory = managerFactory;
        }

        [Import]
        IBusinessManagerFactory _managerFactory;

        public IEnumerable<RoleMaster> Get()
        {
            var securityService = _managerFactory.GetBusinessManager<ISecurityService>();
            return securityService.GetRoles();
        }
       
        public IEnumerable<RoleMaster> Get(int userId)
        {
            var securityService = _managerFactory.GetBusinessManager<ISecurityService>();
            return securityService.GetUserRoles(userId);
        }

        public IEnumerable<RoleMaster> Get(string username)
        {
            var securityService = _managerFactory.GetBusinessManager<ISecurityService>();
            return securityService.GetUserRoles(username);
        }
   }
}
