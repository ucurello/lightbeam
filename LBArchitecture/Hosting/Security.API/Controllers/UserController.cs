﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using LightBeam.Business.Entities.Security;
using System.ComponentModel.Composition;
using Core.Common.Contracts;
using Core.Common.Core;
using LightBeam.Business.Contracts.Security;

namespace LightBeam.SecurityAPI.Controllers
{
    public class UserController : ApiController
    {       
        public UserController()
        {
            if (ObjectBase.Container != null)
                ObjectBase.Container.SatisfyImportsOnce(this);
        }

        public UserController(IBusinessManagerFactory managerFactory) : this()
        {
            _managerFactory = managerFactory;
        }

        [Import]
        IBusinessManagerFactory _managerFactory;

        public IEnumerable<UserMaster> Get()
        {
            var securityService = _managerFactory.GetBusinessManager<ISecurityService>();
            return securityService.GetUsers();
        }

        public UserMaster Get(int id)
        {
            var securityService = _managerFactory.GetBusinessManager<ISecurityService>();
            return securityService.GetUser(id);
        }

        public UserMaster Get(string username, string password)
        {
            var securityService = _managerFactory.GetBusinessManager<ISecurityService>();
            return securityService.GetUserByLogin(username, password);
        }
    }
}
