﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebMatrix.WebData;

namespace SecurityAPI
{
    public class LBMembershipProvider : SimpleMembershipProvider
    {
        public override void Initialize(string name, System.Collections.Specialized.NameValueCollection config)
        {
            base.Initialize(name, config);
            ApplicationName = "API";
        }

        public override bool ValidateUser(string username, string password)
        {
            
            return base.ValidateUser(username, password);
        }
        
    }
}