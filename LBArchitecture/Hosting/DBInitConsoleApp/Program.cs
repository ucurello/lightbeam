﻿using LightBeam.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightBeam.DBInitConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Initializing Database...");
            using (var context = new LBContext())
            {
                context.Database.Initialize(true);
            }
            Console.WriteLine("DB Initialization Done!");
            Console.ReadLine();
        }
    }
}
